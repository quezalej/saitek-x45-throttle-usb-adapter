#include <Joystick.h>

//pin def 
#define pot1in A0 	  //DB-15=6
#define pot2in A8     //DB-15=3
#define rderin A2   //DB-15=13 ProMicro pin4
#define ttlein A7 //DB-15=11 ProMicro pin6
#define sigAin A9  	  //DB-15=2
#define sigBin A1 	  //DB-15=7
#define sigCin A6 	  //DB-15=10
#define sigDin A3 	  //DB-15=14
//DB pin	1	15	12	 9
//resist	1k	20k	100k 1M

//pin defv2


int pot1val;
int pot2val;
int pot1lst;
int pot2lst;

int rderval;
int ttleval;

int sigAval;
int sigBval;
int sigCval;
int sigDval;

int sigAlst;
int sigBlst;
int sigClst;
int sigDlst;

int fna2lst;
int fna3lst;
int fna4lst;

int fnb2lst;
int fnb3lst;
int fnb4lst;

int fnc3lst;
int fnc4lst;

int fnd3lst;
int fnd4lst;


int throttleo;

int ruddera;
int rudderb;
int ruddero;

int aux;
int mod;

float smoothingFactor = 0.7;
float throttlesm = 0;
float pot1sm = 0;
float pot2sm = 0;

const unsigned long DELAY_TIME = 300;
unsigned long lastup = 0;

Joystick_ Joystick(0x05,JOYSTICK_TYPE_JOYSTICK,
  24, 2,                  // Buttons, Hats
  true, true, false,     // X + Y no Z Axis
  false, false, false,   // No Rx Ry Rz
  true, true,          // rudder + throttle
  false, false, false);  // No accelerator brake steering

void setup() {
	Serial.begin(9600);
	
	pinMode(pot1in, INPUT_PULLUP);
	pinMode(pot2in, INPUT_PULLUP);
	pinMode(rderin, INPUT_PULLUP);
	pinMode(ttlein, INPUT_PULLUP);
	pinMode(sigAin, INPUT_PULLUP);
	pinMode(sigBin, INPUT_PULLUP);
	pinMode(sigCin, INPUT_PULLUP);
	pinMode(sigDin, INPUT_PULLUP);
	
	Joystick.begin(false);
	Joystick.setXAxisRange(391, 1024);
	Joystick.setYAxisRange(370, 1024);
	//Joystick.setThrottleRange(200, 1011);
	//Joystick.setRudderRange(1016, 266);
  Joystick.setThrottleRange(0, 1024);
  Joystick.setRudderRange(1024, 0);

  
  sigAlst = analogRead(sigAin);
  sigBlst = analogRead(sigBin);

  int fna2lst = 0;
  int fna1lst = 0;

}

void loop() {
	
	pot1val = analogRead(pot1in);
	pot2val = analogRead(pot2in);
	rderval = analogRead(rderin);
	ttleval = analogRead(ttlein);
	sigAval = analogRead(sigAin);
	sigBval = analogRead(sigBin);
	sigCval = analogRead(sigCin);
	sigDval = analogRead(sigDin);
	
  unsigned long currentMillis = millis();

	if (sigAval < 1005 && sigAval > 960 && sigCval > 900){
		mod = 3;
	}
	if (sigBval >= 1005 && sigAval >= 1005) {
		mod = 2;
	}
	if (sigBval < 1005 && sigBval > 960){
		mod = 1;
	}
  

//SIGNAL A

if (sigAval >= 965 && sigAval <= 1000) {
    // call function fn1
    fna1();
  }

  // check if the current value has decreased to between 740 and 780:
  if (sigAval >= 740 && sigAval <= 795) {
    if (sigAlst > 1000 || (sigAlst >= 390 && sigAlst <= 480) ) {
      fna2a();
    } else if (sigAlst >= 965 && sigAlst <= 1000) {
      fna2b();
    } else if (sigAlst >= 740 && sigAlst <= 795){
	  if (fna2lst == 0) {fna2a();}
      if (fna2lst == 1) {fna2b();}
    }
	}

  // check if the current value has decreased to between 400 and 470:
  if (sigAval >= 400 && sigAval <= 480) {
    if (sigAlst > 1000 || (sigAlst >= 390 && sigAlst <= 400) || (sigAlst >= 145 && sigAlst <= 200)) {
      fna3a();
    } else if (sigAlst >= 965 && sigAlst <= 1000) {
      fna3b();
    } else if (sigAlst >= 740 && sigAlst <= 795) {
      fna3c();
	} else if (sigAlst >= 400 && sigAlst <= 480){
      if (fna3lst == 0){fna3a();}
	  if (fna3lst == 1){fna3b();}
      if (fna3lst == 2){fna3c();}
    }
  }
if (sigAval >= 390 && sigAval <= 400) {
      fna3d();
  }

  // check if the current value has decreased to between 145 and 160:
  if (sigAval >= 145 && sigAval <= 200) {
    if (sigAlst > 1000) {
      // call function fn4a
      fna4a();
    } else if (sigAlst >= 965 && sigAlst <= 1000) {
      // call function fn4b
      fna4b();
    } else if (sigAlst >= 400 && sigAlst <= 480) {
      // call function fn4c
      fna4c();
    } else if (sigAlst >= 145 && sigAlst <= 200){
      if (fna4lst == 0){fna4a();}
	  if (fna4lst == 1){fna4b();}
      if (fna4lst == 2){fna4c();}
    }
  }


//SIGNAL B

if (sigBval >= 965 && sigBval <= 1000) {
    // call function fn1
    fnb1();
  }

  // check if the current value has decreased to between 740 and 780:
  if (sigBval >= 740 && sigBval <= 795) {
    if (sigBlst > 1000 || (sigBlst >= 390 && sigBlst <= 480) ) {
      fnb2a();
    } else if (sigBlst >= 965 && sigBlst <= 1000) {
      fnb2b();
    } else if (sigBlst >= 740 && sigBlst <= 795){
	  if (fnb2lst == 0) {fnb2a();}
      if (fnb2lst == 1) {fnb2b();}
    }
	}

  // check if the current value has decreased to between 400 and 470:
  if (sigBval >= 400 && sigBval <= 480) {
    if (sigBlst > 1000 || (sigBlst >= 390 && sigBlst <= 400) || (sigBlst >= 145 && sigBlst <= 200)) {
      fnb3a();
    } else if (sigBlst >= 965 && sigBlst <= 1000) {
      fnb3b();
    } else if (sigBlst >= 740 && sigBlst <= 795) {
      fnb3c();
	} else if (sigBlst >= 400 && sigBlst <= 480){
      if (fnb3lst == 0){fnb3a();}
	  if (fnb3lst == 1){fnb3b();}
      if (fnb3lst == 2){fnb3c();}
    }
  }
if (sigBval >= 390 && sigBval <= 400) {
      fnb3d();
  }

  // check if the current value has decreased to between 145 and 160:
  if (sigBval >= 145 && sigBval <= 200) {
    if (sigBlst > 1000) {
      // call function fn4a
      fnb4a();
    } else if (sigBlst >= 965 && sigBlst <= 1000) {
      // call function fn4b
      fnb4b();
    } else if (sigBlst >= 400 && sigBlst <= 480) {
      // call function fn4c
      fnb4c();
    } else if (sigBlst >= 145 && sigBlst <= 200){
      if (fnb4lst == 0){fnb4a();}
	  if (fnb4lst == 1){fnb4b();}
      if (fnb4lst == 2){fnb4c();}
    }
  }

//SIGNAL C

if (sigCval >= 965 && sigCval <= 1000) {
    // call function fn1
    fnc1();
  }
  
  if (sigCval >= 400 && sigCval <= 480) {
    if (sigClst > 1000 || (sigClst >= 390 && sigClst <= 440) || (sigClst >= 145 && sigClst <= 200)) {
      fnc3a();
    } else if (sigClst >= 965 && sigClst <= 1000) {
      fnc3b();
    } else if (sigClst >= 400 && sigClst <= 480){
      if (fnc3lst == 0){fnc3a();}
	  if (fnc3lst == 1){fnc3b();}
    }
  }

  if (sigCval >= 145 && sigCval <= 200) {
    if (sigClst > 1000) {
      // call function fn4a
      fnc4a();
    } else if (sigClst >= 965 && sigClst <= 1000) {
      // call function fn4b
      fnc4b();
    } else if (sigClst >= 400 && sigClst <= 480) {
      // call function fn4c
      fnc4c();
    } else if (sigClst >= 145 && sigClst <= 200){
      if (fnc4lst == 0){fnc4a();}
	  if (fnc4lst == 1){fnc4b();}
      if (fnc4lst == 2){fnc4c();}
    }
  }

//SIGNAL D

if (sigDval >= 965 && sigDval <= 1000) {
    // call function fn1
    fnd1();
  }
  
  if (sigDval >= 400 && sigDval <= 480) {
    if (sigDlst > 1000 || (sigDlst >= 390 && sigDlst <= 440) || (sigDlst >= 145 && sigDlst <= 200)) {
      fnd3a();
    } else if (sigDlst >= 965 && sigDlst <= 1000) {
      fnd3b();
    } else if (sigDlst >= 400 && sigDlst <= 480){
      if (fnd3lst == 0){fnd3a();}
	  if (fnd3lst == 1){fnd3b();}
    }
  }

  if (sigDval >= 145 && sigDval <= 200) {
    if (sigDlst > 1000) {
      // call function fn4a
      fnd4a();
    } else if (sigDlst >= 965 && sigDlst <= 1000) {
      // call function fn4b
      fnd4b();
    } else if (sigDlst >= 400 && sigDlst <= 480) {
      // call function fn4c
      fnd4c();
    } else if (sigDlst >= 145 && sigDlst <= 200){
      if (fnd4lst == 0){fnd4a();}
	  if (fnd4lst == 1){fnd4b();}
      if (fnd4lst == 2){fnd4c();}
    }
  }
	
	//Axis
  if (currentMillis - lastup >= DELAY_TIME) {
  if (pot1val - pot1lst >= 20) {Joystick.setButton(14, 1);}
  if (pot1lst - pot1val >= 20) {Joystick.setButton(15, 1);}
  pot1lst = pot1val;

  if (pot2val - pot2lst >= 20) {Joystick.setButton(16, 1);}
  if (pot2lst - pot2val >= 20) {Joystick.setButton(17, 1);}
  pot1lst = pot1val;
  pot2lst = pot2val;
  lastup = currentMillis;
  }


	/*
  	
	pot1sm = (smoothingFactor * pot1val) + ((1 - smoothingFactor) * pot1sm);
	pot2sm = (smoothingFactor * pot2val) + ((1 - smoothingFactor) * pot2sm);
  if (pot1val >= 715 || pot1val <=700){Joystick.setXAxis(pot1sm);}
	else {Joystick.setXAxis(512);}
	if (pot2val >= 715 || pot2val <=700){Joystick.setYAxis(pot2sm);}
	else {Joystick.setYAxis(512);}*/
	
  //rudder map
  if (rderval < 700){
	  ruddero = map(rderval, 240, 700, 0, 512);
  }
  else if (rderval > 720){
	  ruddero = map(rderval, 720, 1000, 512, 1024);
  }
  else{
	  ruddero = 512;
  }
	Joystick.setRudder(ruddero);

  //throttle map
  
  throttlesm = (smoothingFactor * ttleval) + ((1 - smoothingFactor) * throttlesm);
  
  if (mod == 1){
	  throttleo = map(throttlesm, 200, 750, 512, 1024);
  }
  else if (mod == 3){
	  throttleo = map(throttlesm, 200, 1010, 0, 1024);
  }
  else{
	  throttleo = throttlesm;
  }
  Joystick.setThrottle(throttleo);
  Joystick.sendState();

	delay(50);
    for (int i=0; i<24;i++){
    Joystick.setButton(i, 0);
  }
  Serial.print(sigAval);
  Serial.print(",");
  Serial.println(sigAlst);
  Serial.print(sigBval);
  Serial.print(",");
  Serial.println(sigBlst);
  Serial.print(sigCval);
  Serial.print(",");
  Serial.println(sigClst);
  Serial.print(sigDval);
  Serial.print(",");
  Serial.println(sigDlst);
  sigAlst = sigAval;
  sigBlst = sigBval;
  sigClst = sigCval;
  sigDlst = sigDval;
}


//fnA
void fna1() {
  Serial.println("fna1");
  //Joystick.setButton(19, 1);
}
void fna2a() {
  Serial.println("fna2a");
	Joystick.setButton(0, 1);
  fna2lst = 0;
}

void fna2b() {
  Serial.println("fna2b");
  Joystick.setButton(0, 1);
 // Joystick.setButton(19, 1);
  fna2lst = 1;
}

void fna3a() {
  Serial.println("fna3a");
  fna3lst = 0;
  Joystick.setButton(5, 1);
}

void fna3b() {
  Serial.println("fna3b");
  fna3lst = 1;
 // Joystick.setButton(19, 1);
  Joystick.setButton(5, 1);
}

void fna3c() {
  Serial.println("fna3c");
  fna3lst = 2;
  Joystick.setButton(0, 1);
  Joystick.setButton(5, 1);
}

void fna3d() {
  Serial.println("fna3d");
  fna3lst = 3;
  Joystick.setButton(0, 1);
  Joystick.setButton(5, 1);
}

void fna4a() {
  Serial.println("fna4a");
  fna4lst = 0;
  Joystick.setButton(9, 1);
}

void fna4b() {
  Serial.println("fna4b");
  fna4lst = 1;
 // Joystick.setButton(19, 1);
  Joystick.setButton(9, 1);
}

void fna4c() {
  Serial.println("fnac4");
  fna4lst = 2;
  Joystick.setButton(5, 1);
  Joystick.setButton(9, 1);
}

//fnB
void fnb1() {
  Serial.println("fnb1");
  //Joystick.setButton(20, 1);
}
void fnb2a() {
  Serial.println("fnb2a");
	Joystick.setButton(1, 1);
  fnb2lst = 0;
}

void fnb2b() {
  Serial.println("fnb2b");
  Joystick.setButton(1, 1);
 // Joystick.setButton(20, 1);
  fnb2lst = 1;
}

void fnb3a() {
  Serial.println("fnb3a");
  fnb3lst = 0;
  Joystick.setButton(6, 1);
}

void fnb3b() {
  Serial.println("fnb3b");
  fnb3lst = 1;
 // Joystick.setButton(20, 1);
  Joystick.setButton(6, 1);
}

void fnb3c() {
  Serial.println("fnb3c");
  fnb3lst = 2;
  Joystick.setButton(1, 1);
  Joystick.setButton(6, 1);
}

void fnb3d() {
  Serial.println("fnb3d");
  fnb3lst = 3;
  Joystick.setButton(1, 1);
  Joystick.setButton(6, 1);
}

void fnb4a() {
  Serial.println("fnb4a");
  fnb4lst = 0;
  Joystick.setButton(10, 1);
}

void fnb4b() {
  Serial.println("fnb4b");
  fnb4lst = 1;
 // Joystick.setButton(20, 1);
  Joystick.setButton(10, 1);
}

void fnb4c() {
  Serial.println("fnbc4");
  fnb4lst = 2;
  Joystick.setButton(6, 1);
  Joystick.setButton(10, 1);
}

///C

void fnc1() {
  Serial.println("fnc1");
  Joystick.setButton(21, 1);
}

void fnc3a() {
  Serial.println("fnc3a");
  fnc3lst = 0;
  Joystick.setButton(3, 1);
}

void fnc3b() {
  Serial.println("fnc3b");
  fnc3lst = 1;
  Joystick.setButton(21, 1);
  Joystick.setButton(3, 1);
}

void fnc4a() {
  Serial.println("fnc4a");
  fnc4lst = 0;
  Joystick.setButton(7, 1);
}

void fnc4b() {
  Serial.println("fnc4b");
  fnc4lst = 1;
  Joystick.setButton(21, 1);
  Joystick.setButton(7, 1);
}

void fnc4c() {
  Serial.println("fncc4");
  fnc4lst = 2;
  Joystick.setButton(3, 1);
  Joystick.setButton(7, 1);
}

//D

void fnd1() {
  Serial.println("fnd1");
  Joystick.setButton(22, 1);
}

void fnd3a() {
  Serial.println("fnd3a");
  fnd3lst = 0;
  Joystick.setButton(4, 1);
}

void fnd3b() {
  Serial.println("fnd3b");
  fnd3lst = 1;
  Joystick.setButton(22, 1);
  Joystick.setButton(4, 1);
}

void fnd4a() {
  Serial.println("fnd4a");
  fnd4lst = 0;
  Joystick.setButton(8, 1);
}

void fnd4b() {
  Serial.println("fnd4b");
  fnd4lst = 1;
  Joystick.setButton(22, 1);
  Joystick.setButton(8, 1);
}

void fnd4c() {
  Serial.println("fndc4");
  fnd4lst = 2;
  Joystick.setButton(4, 1);
  Joystick.setButton(8, 1);
}