# Saitek X45 Throttle to USB adapter

## Background
The Saitek X45 Flight Control System was released in 2002, and included joystick and throttle peripherals. The joystick connects to a computer via a USB cable, and the throttle connects to said joystick via a DB-15 cable. This renders the throttle useless without the joystick to provide interfacing with the computer.
Since I already have a flight stick, I decided to purchase an X45 throttle from eBay. I knew beforehand that I would have to figure out a way to connect it to the computer, since the throttle itself has no interfacing capabilities.

## Construction and circuitry
After disassembling the throttle, it became apparent that the joystick was performing all of the processing, as the boards present in the throttle only contain diodes and input components (potentiometers, buttons and switches). At this point the challenge was tracing all the lines to their respective circuits, so I put together a schematic of the PCB using KiCad.

- The PCB was covered in hot glue, which made it difficult to see what was going on underneath it

![pcb](IMAGES/pcb.jpg)

- Schematic of the PCB created in KiCad

![schematic](IMAGES/schematic.png)

I noticed that some of the wires that reach the DB-15 pins carry signals from up to 4 different inputs, this being a combination of switches, sticks and buttons.
From looking at the PCB traces, I noticed all of the inputs that share a signal line have separate grounds, so I imagine the joystick was providing a different voltage to each of these inputs to discriminate between them. To achieve a behavior similar to this, I added a resistor to the ground lines of each of these inputs to modify the voltage that they report when shorted. I had to use the analog pins in an Arduino Pro Micro to read the voltage as an analog value ranging from 0 to 1023.
To choose resistors, I tried random combinations and noted the results in a spreadsheet. This allowed me to know the value ranges that are expected from each input, as well as the combinations of them. For example: if Button A and Button B share a signal line, I have to make sure they provide a different voltage so the analog read value can tell me which of the two is being pressed, but I also need to account for both of them being pressed simultaneously which will give another analog value.

- Voltage measure with a variety of resistor values:

![Resistor Values](IMAGES/resistorvalues.png)

- Analog read values reported by the Arduino board when using the selected resistors (1M, 100k, 20k and 1k):

![Analog Values](IMAGES/analogvalues.png)

Since I placed the resistors on the female DB-15 connector I had no need for separate ground lines, so not all the DB-15 pins are specified. An Arduino Pro Micro is required due to the amount of analog pins needed. The sketch uses the following pinout:
- Arduino Pin A0 = Potentiometer 2 signal, connects to the DB-15 pin #6
- Arduino Pin A8 = Potentiometer 1 signal, connects to the DB-15 pin #3
- Arduino Pin A2 = Rudder axis signal, connects to the DB-15 pin #13
- Arduino Pin A7 = Throttle axis signal, connects to the DB-15 pin #11
- Arduino Pin A9 = Multi signal A, connects to the DB-15 pin #2
- Arduino Pin A1 = Multi signal B, connects to the DB-15 pin #7
- Arduino Pin A6 = Multi signal C, connects to the DB-15 pin #10
- Arduino Pin A3 = Multi signal D, connects to the DB-15 pin #14

Testing the pinout and resistor combination on a breadboard:

![Testing setup](IMAGES/testing.jpg)

## Sketch information
The Arduino [program](SaitekX45V2.2.ino) uses the Joystick.h library to interface with a Windows computer as a regular joystick.
It is configured to use the X and Y joystick axis mapped to the two potentiometers, and the throttle and rudder are mapped to their respective axis.

The program detects changes in the current readings for all the inputs, and forwards this information once per loop. This assures that no commands will be interrupted. I coded an Exponential Moving Average (EMA) section to smooth the analog reads in case of jitter, but it was not necessary for me to use this hardware. The relevant lines can be uncommented and the EMA factor configured in the code.

The analog read values that the program expects are determined by the resistors used, so this might be variable depending on the hardware configuration. Fine tuning these values is recommended if there is any change in hardware.
Once the program detects a value that falls in the ranges calculated beforehand, it will call a function that changes the status of the respective button from released to pressed, and will maintain this status while the button is held. The loop code defaults to releasing all the buttons that are not actively pressed to avoid having any ghost inputs.

On V2.2 the simultaneous button detection was improved, as a unique function for each combination of buttons was the most reliable way of detecting said combinations accurately. There are still some bugs inherent to the electrical properties of the hardware and occasionally when holding two buttons and releasing, only one the output will not update accordingly and it will behave as if both buttons were still held.